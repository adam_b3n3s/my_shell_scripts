#!/bin/bash

# Function to sort and remove duplicates from package lists
clean_list() {
    sort -u
}

echo "Packages installed with yay:"
yay -Qqe | clean_list > yay_packages.txt
yay -Qqe

echo "Packages installed with pacman:"
pacman -Qqe | clean_list > pacman_packages.txt
pacman -Qqe

echo "Checking for any differences..."

# Compare the two lists
if diff -q yay_packages.txt pacman_packages.txt >/dev/null; then
    echo "No differences found. All packages installed with yay and pacman are the same."
else
    echo "Differences found:"
    echo "Packages installed only with yay:"
    comm -23 yay_packages.txt pacman_packages.txt
    echo "Packages installed only with pacman:"
    comm -13 yay_packages.txt pacman_packages.txt
fi

# Clean up temporary files
rm ./yay_packages.txt ./pacman_packages.txt

yay_count=$(yay -Qqe | clean_list | wc -l)
echo "Number of packages installed with yay: $yay_count"

pacman_count=$(pacman -Qqe | clean_list | wc -l)
echo "Number of packages installed with pacman: $pacman_count"
