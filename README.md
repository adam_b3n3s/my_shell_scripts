# Bin

This repository contains a collection of useful shell scripts (made by me) for various tasks.

## Installation

1. Clone this repository to your local machine:

    ```bash
    git clone https://github.com/adam_b3n3s/shell_scripts.git
    ```

2. Navigate to the directory containing the scripts:

    ```bash
    cd shell-scripts
    ```

3. Move the desired script(s) to a directory in your PATH. For example, to move the `run.sh` script to `/usr/local/bin`, you can use:

    ```bash
    mv run /usr/local/bin/
    ```

    If you prefer to use a different directory, feel free to do so. Just ensure that the directory is included in your PATH environment variable.

4. Optionally, you can add the scripts directory to your PATH to make it accessible from anywhere. This can be done by adding the following line to your shell configuration file (`~/.bashrc` for Bash or `~/.zshrc` for Zsh):

    ```bash
    export PATH="$PATH:/path/to/shell-scripts"
    ```

    Don't forget to replace `/path/to/shell-scripts` with the actual path to your scripts directory. After adding this line, remember to source the file to apply the changes:

    ```bash
    source ~/.bashrc   # or ~/.zshrc if you're using zsh
    ```

## Usage

Once the script is installed and accessible from your PATH, you can use it from any directory by typing its name followed by any arguments it requires. For example:

```bash
run arduino
